import java.util.*;



public class CollectionsHomeWork {
    public static void main(String[] args)  {
        List<String> list = Arrays.asList("Antonov", "Shevchenko", "Boyko", "Shevchenko", "Kravchenko", "Savitskyi", "Antonov", "Shevchenko", "Teterov", "Mamchenko");
        for (String lastnameArray : list) {
            System.out.println("ArrayList: " + lastnameArray);
        }

        Set<String> setLastnames = Set.of("Antonov", "Shevchenko", "Boyko", "Kravchenko", "Savitskyi", "Teterov","Mamchenko" );
        for (String setLastname : setLastnames) {
            System.out.println("Set: " + setLastname);
        }

        Map<String, String> lastnameSpell = Map.of("Antonov", "7 letters", "Shevchenko", "10 letters", "Boyko", "5 letters", "Kravchenko", "10 letters", "Savitskyi", "9 letters", "Teterov", "5 letters", "Mamchenko", "9 letters");
        for (Map.Entry<String, String> entry: lastnameSpell.entrySet())
            System.out.println(entry.getKey() + " - " + entry.getValue());
    }}

